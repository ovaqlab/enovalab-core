<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Models\User;
use Faker\Generator as Faker;
use Illuminate\Support\Str;

/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| This directory should contain each of the model factory definitions for
| your application. Factories provide a convenient way to generate new
| model instances for testing / seeding your application's database.
|
*/

$factory->define(User::class, function (Faker $faker) {
    return [
        'user_id' => $faker->uuid,
        'first_name' => $faker->firstName,
        'last_name' => $faker->lastName,
        'email' => "jithin@linways.com",
        'email_verified_at' => now(),
        'password' => bcrypt("jithin"),
        'phone_number' => $faker->phoneNumber,
        'alternate_phone_number' => $faker->phoneNumber,
        'employee_code' => $faker->unique()->postcode,
        'address' => $faker->address,
        'nationality' => $faker->country,
        'dob' => $faker->date('Y-m-d'),
        'remember_token' => Str::random(10),
        'gender_id' => rand(2, 4),
        'activation_token' => Str::random(16)

    ];
});
