<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateUsers extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users', function (Blueprint $table) {
            $table->uuid("user_id")->primary();
            $table->string('employee_code')->unique();
            $table->string('first_name');
            $table->string('last_name');
            $table->string('address');
            $table->string('phone_number');
            $table->string('alternate_phone_number')->nullable();
            $table->string('nationality');
            $table->string('email')->unique();
            $table->string('password');
            $table->date('dob');
            $table->string("pan_number")->nullable()->unique();
            $table->unsignedInteger("gender_id");
            $table->boolean('is_resigned')->default(false);
            $table->boolean('is_suspended')->default(false);
            $table->boolean("is_active")->default(0);
            $table->json('related_urls')->nullable();
            $table->json('qualifications')->nullable();
            $table->json('experience')->nullable();
            $table->timestamp('email_verified_at')->nullable();
            $table->string('avatar')->default('avatar.png');
            $table->string('activation_token');
            $table->uuid('updated_by')->nullable();
            $table->uuid('created_by')->nullable();
            $table->timestamps();
            $table->rememberToken();
            $table->softDeletes();
            $table->foreign('gender_id')->references('genders')->on('gender_id');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('users');
    }
}
