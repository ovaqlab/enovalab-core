<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateAuthUserPermissions extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('auth_user_permissions', function (Blueprint $table) {
            $table->bigIncrements('user_permission_id');
            $table->uuid('user_id');
            $table->unsignedBigInteger('permission_id');
            $table->dateTime("created_date")->nullable();
            $table->dateTime("updated_date")->nullable();
            $table->uuid('updated_by')->nullable();
            $table->uuid('created_by')->nullable();
            $table->unique(['user_id', 'permission_id']);
            $table->foreign('permission_id')->references('permission_id')->on('auth_permissions');
            $table->foreign('user_id')->references('user_id')->on('users');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('auth_user_permissions');
    }
}
