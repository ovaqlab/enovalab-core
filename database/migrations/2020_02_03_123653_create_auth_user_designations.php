<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateAuthUserDesignations extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('auth_user_designations', function (Blueprint $table) {
            $table->bigIncrements('user_designation_id');
            $table->uuid('user_id');
            $table->unsignedBigInteger('designation_id');
            $table->foreign('designation_id')->references('designation_id')->on('designations');
            $table->foreign('user_id')->references('user_id')->on('users');
            $table->dateTime("created_date")->nullable();
            $table->dateTime("updated_date")->nullable();
            $table->boolean("is_active")->default(true);
            $table->uuid('updated_by')->nullable();
            $table->uuid('created_by')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('auth_user_designations');
    }
}
