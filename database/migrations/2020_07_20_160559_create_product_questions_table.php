<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateProductQuestionsAndAnswersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('product_questions', function (Blueprint $table) {
            $table->bigIncrements('product_question_id');
            $table->unsignedBigInteger('product_id');
            $table->string('question', 255);
            $table->uuid('asked_by');
            $table->boolean('is_active')->default(true);
            $table->foreign('product_id')->references('product_id')->on('products');
            $table->foreign('asked_by')->references('user_id')->on('users');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('product_questions_and_answers');
    }
}
