<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateAuthUserRoles extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('auth_user_roles', function (Blueprint $table) {
            $table->bigIncrements('user_role_id');
            $table->uuid('user_id');
            $table->unsignedBigInteger('role_id');
            $table->dateTime("created_date")->nullable();
            $table->dateTime("updated_date")->nullable();
            $table->boolean("is_active")->default(true);
            $table->uuid('updated_by')->nullable();
            $table->uuid('created_by')->nullable();
            $table->foreign('user_id')->references('user_id')->on('users');
            $table->foreign('role_id')->references('role_id')->on('roles');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('auth_user_roles');
    }
}
