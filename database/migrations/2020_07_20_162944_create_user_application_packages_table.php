<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateUserApplicationPackagesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('user_application_packages', function (Blueprint $table) {
            $table->unsignedBigInteger('user_application_package_id');
            $table->uuid('user_id');
            $table->unsignedBigInteger('application_package_id');
            $table->dateTime('purchased_on');
            $table->foreign('user_id')->references('user_id')->on('users');
            $table->foreign('application_package_id')->references('application_package_id')->references('application_packages');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('user_application_packages');
    }
}
