<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateProductQuestionAnswersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('product_question_answers', function (Blueprint $table) {
            $table->bigIncrements('product_question_answer_id');
            $table->unsignedBigInteger('product_question_id');
            $table->string('answer', 255);
            $table->uuid('answered_by');
            $table->boolean('is_active')->default(true);
            $table->foreign('product_question_id')->references('product_question_id')->on('product_questions');
            $table->foreign('asked_by')->references('user_id')->on('users');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('product_question_answers');
    }
}
