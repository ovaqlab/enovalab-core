<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateAuthPermissions extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('auth_permissions', function (Blueprint $table) {
            $table->bigIncrements('permission_id');
            $table->string("name")->unique();
            $table->string("code")->unique();
            $table->dateTime("created_date")->nullable();
            $table->dateTime("updated_date")->nullable();
            $table->boolean("is_active")->default(true);
            $table->uuid('updated_by')->nullable();
            $table->uuid('created_by')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('auth_permissions');
    }
}
