<?php

namespace App\Http\Controllers\API;

use App\Models\User;
use App\Notifications\SignUpActivate;
use Avatar;
use Illuminate\Http\Request;
use Ramsey\Uuid\Uuid;
use Storage;

class UserController extends BaseController
{
    /**
     * @return \Illuminate\Http\JsonResponse
     * @author jithinvijayan
     */
    public function index()
    {
        $users = User::all();
        return $this->sendResponse($users->toArray(), "Users received successfully");
    }

    public function addTeamMember(Request $request)
    {
        $request->validate([
            'firstName' => 'required|string',
            'email' => 'required|email|unique:users',
            'password' => 'required|string|confirmed',
            'phoneNumber' => 'required|string|min:10',
            'alternatePhoneNumber' => 'string|min:10',
            'gender' => 'required|exists:genders,gender_id',
            'address' => 'required|string|min:10',
            'dateOfBirth' => 'required|date|date_format:d-m-Y',
            'panNumber' => 'required|string|min:6',
            'nationality' => 'required|exists:nationalities,nationality_id'
        ]);
        $userId = Uuid::uuid4()->toString();
        $user = new User([
            'user_id' => $userId,
            'first_name' => $request->firstName,
            'last_name' => $request->lastName,
            'email' => $request->email,
            'phone_number' => $request->phoneNumber,
            'alternate_phone_number' => $request->alternatePhoneNumber,
            'address' => $request->address,
            'nationality' => $request->nationality,
            'dob' => $request->dateOfBirth,
            'pan_number' => $request->panNumber,
            'gender_id' => $request->gender,
            'password' => bcrypt(str_random(8)),
            'activation_token' => str_random(60)
        ]);
        $user->save();
        $avatar = Avatar::create($user->name)->getImageObject()->encode('png');
        Storage::put('avatars/' . $userId . '/avatar.png', (string)$avatar);
        $user->notify(new SignUpActivate($user));
        return response()->json([
            'message' => __('auth.signup_success')
        ], 201);
    }
}
