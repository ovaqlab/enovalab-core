<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class CredentialsController extends BaseController
{
    public function getClientDetails()
    {
        $response = DB::table('oauth_clients')
            ->select(array('id', 'secret'))
            ->where('password_client', 1)
            ->get()
            ->first();
        return $this->sendResponse($response, 'Credentials retrieved successfully.');
    }
}
