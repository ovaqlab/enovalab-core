<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class BaseController extends Controller
{
    /**
     * @param $result
     * @param $message
     * @return \Illuminate\Http\JsonResponse
     * @author jithinvijayan
     */
    public function sendResponse($result, $message)
    {
        $response = [
            'success' => true,
            'data' => $result,
            'message' => $message
        ];
        return response()->json($response, 200);
    }


    /**
     * @param $error
     * @param array $errorMessages
     * @param int $code
     * @return \Illuminate\Http\JsonResponse
     * @author jithinvijayan
     */
    public function sendError($error, $errorMessages = [], $code = 404)
    {
        $response = [
            "success" => false,
            'message' => $errorMessages
        ];
        return response()->json($response, $code);
    }

    public function sendJsonError($error, $errorMessages = [])
    {
        $response = [
            "success" => false,
            "error" => $error
        ];
        if (!empty($errorMessages)) {
            $response['messages'] = $errorMessages;
        }
        return response()->json($response, 200);

    }
}
