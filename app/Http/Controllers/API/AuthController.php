<?php

namespace App\Http\Controllers\API;

use App\Http\Resources\UserResource;
use App\Models\User;
use App\Notifications\SignUpActivate;
use App\Notifications\SignUpActivated;
use Avatar;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Storage;

class AuthController extends BaseController
{
    public function signUp(Request $request)
    {
        $request->validate([
            'firstName' => 'required|string',
            'email' => 'required|email|unique:users',
            'password' => 'required|string|confirmed'
        ]);

        $user = new User([
            'first_name' => $request->firstName,
            'last_name' => $request->lastName,
            'email' => $request->email,
            'password' => bcrypt($request->password),
            'activation_token' => str_random(60)
        ]);
        $user->save();
        $avatar = Avatar::create($user->name)->getImageObject()->encode('png');
        Storage::put('avatars/' . $user->id . '/avatar.png', (string)$avatar);

        $user->notify(new SignUpActivate($user));

        return response()->json([
            'message' => __('auth.signup_success')
        ], 201);
    }

    /**
     * @param $token
     * @return \Illuminate\Http\JsonResponse
     * @author jithinvijayan
     */
    public function signUpActivate($token)
    {
        $user = User::where('activation_token', $token)->first();

        if (!$user) {
            return response()->json([
                'message' => __('auth.token_invalid')
            ], 404);
        }

        $user->active = true;
        $user->activation_token = '';
        $user->save();

        $user->notify(new SignUpActivated($user));

        return $user;
    }

    public function login(Request $request)
    {
        $request->validate([
            'email' => 'required|string|email',
            'password' => 'required|string',
            'remember_me' => 'boolean'
        ]);

        $credentials = request(['email', 'password']);
        $credentials['is_active'] = 1;
        $credentials['deleted_at'] = null;
        if (!Auth::attempt($credentials))
            return response()->json([
                'message' => __('auth.login_failed')
            ], 401);

        $user = $request->user();
        $userEmail = $request->input('email');
        $userObject = User::where('email', '=', $userEmail)->get(['user_id', 'first_name', 'last_name']);
        if (is_null($userObject)) {
            return $this->sendError("Invalid user name or password");
        }
        $userObject = new UserResource($userObject[0]);
        $tokenResult = $user->createToken('Personal Access Token');
        $token = $tokenResult->token;
        if ($request->remember_me)
            $token->expires_at = Carbon::now()->addWeeks(1);
        else
            $token->expires_at = Carbon::now()->addHours(24);
        $token->save();
        return $this->sendResponse([
            'accessToken' => $tokenResult->accessToken,
            'tokenType' => 'Bearer',
            'userObject' => $userObject,
            'expiresAt' => Carbon::parse($tokenResult->token->expires_at)->toDateTimeString()
        ], 'User logged in successfully.');
    }

    /**
     * Logout user (Revoke the token)
     *
     * @return [string] message
     */
    public function logout(Request $request)
    {
        $request->user()->token()->revoke();

        return response()->json([
            'message' => __('auth.logout_success')
        ]);
    }

    /**
     * Get the authenticated User
     *
     * @return [json] user object
     */
    public function user(Request $request)
    {
        return response()->json($request->user());
    }
}
