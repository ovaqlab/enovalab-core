<?php


namespace App\Http\Controllers\API;

use App\Http\Resources\GenderResource;
use App\Http\Resources\NationalityResource;
use App\Models\Gender;
use App\Models\Nationality;

/**
 *
 * @author jithinvijayan
 * @Date 07/03/20
 */
class HelperController extends BaseController
{
    /**
     * Fetching gender details
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function getGenders()
    {
        try {
            $genders = Gender::all()->where('is_active', "=", 1);
            if (is_null($genders)) {
                return $this->sendError("No genders added to the database");
            }
            return $this->sendResponse(GenderResource::collection($genders), "Genders retrieved successfully");
        } catch (\Exception $e) {
            return $this->sendJsonError($e, "Error occurred while fetching the genders list");
        }

    }

    /**
     * Fetching nationalities
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function getNationalities()
    {
        try {
            $nationalities = Nationality::all()->where('is_active', "=", 1);
            if (is_null($nationalities)) {
                return $this->sendError("No nationality added to the database");
            }
            return $this->sendResponse(NationalityResource::collection($nationalities), "Nationalities retrieved successfully");
        } catch (\Exception $e) {
            return $this->sendJsonError($e, "Error occurred while fetching the nationalities");
        }

    }
}
