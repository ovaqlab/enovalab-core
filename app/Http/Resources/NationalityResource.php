<?php


namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

/**
 *
 * @Date 14/04/20
 * @author  JithinVijayan <jithinvijayanofficial@gmail.com>
 * @license MIT kafka-php/LICENSE.md
 */
class NationalityResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param \Illuminate\Http\Request $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            "id" => $this->nationality_id,
            "name" => $this->name,
            "code" => $this->code
        ];
    }
}
