<?php


namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

/**
 *
 * @Date 09/04/20
 * @author  JithinVijayan <jithinvijayanofficial@gmail.com>
 * @license MIT kafka-php/LICENSE.md
 */
class UserResource extends JsonResource
{

    public function toArray($request)
    {
        return [
            'userId' => $this->user_id,
            'firstName' => $this->first_name,
            'lastName' => $this->last_name
        ];
    }

}
