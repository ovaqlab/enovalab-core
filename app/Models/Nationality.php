<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Notifications\Notifiable;
use Laravel\Passport\HasApiTokens;

class Nationality extends Model
{
    use HasApiTokens, Notifiable;

    /**
     * @var string
     */
    protected $primaryKey = 'nationality_id';

    /**
     * @var array
     */
    protected $fillable = ['name', 'code', 'created_at', 'updated_at', 'is_active'];

    public function users()
    {
        return $this->hasMany(User::class);
    }
}
