<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Notifications\Notifiable;
use Laravel\Passport\HasApiTokens;

/**
 * App\Models\Designation
 *
 * @property int $designation_id
 * @property string $name
 * @property string $description
 * @property int $is_active
 * @property string|null $updated_by
 * @property string|null $created_by
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property-read \Illuminate\Database\Eloquent\Collection|\Laravel\Passport\Client[] $clients
 * @property-read int|null $clients_count
 * @property-read \Illuminate\Notifications\DatabaseNotificationCollection|\Illuminate\Notifications\DatabaseNotification[] $notifications
 * @property-read int|null $notifications_count
 * @property-read \Illuminate\Database\Eloquent\Collection|\Laravel\Passport\Token[] $tokens
 * @property-read int|null $tokens_count
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\User[] $users
 * @property-read int|null $users_count
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Designation newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Designation newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Designation query()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Designation whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Designation whereCreatedBy($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Designation whereDescription($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Designation whereDesignationId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Designation whereIsActive($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Designation whereName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Designation whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Designation whereUpdatedBy($value)
 * @mixin \Eloquent
 */
class Designation extends Model
{
    use HasApiTokens, Notifiable;
    /**
     * @var string
     */
    protected $primaryKey = "designation_id";

    /**
     * @var array
     */
    protected $fillable = ['name', 'description', 'updated_by', 'created_by', 'is_active'];

    public function users()
    {
        return $this->hasMany('App\Models\User');
    }
}
