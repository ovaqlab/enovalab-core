<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Notifications\Notifiable;
use Laravel\Passport\HasApiTokens;

class Gender extends Model
{
    use HasApiTokens, Notifiable;

    /**
     * @var string
     */
//    protected $primaryKey = 'gender_id';

    /**
     * @var array
     */
    protected $fillable = ['name', 'description', 'updated_by', 'created_by', 'is_active'];

    public function users()
    {
        return $this->hasMany(User::class);
    }
}
