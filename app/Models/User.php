<?php

namespace App\Models;

use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Laravel\Passport\HasApiTokens;
use Storage;

/**
 * App\Models\User
 *
 * @property string $user_id
 * @property string $employee_code
 * @property string $first_name
 * @property string $last_name
 * @property string $address
 * @property string $phone_number
 * @property string|null $alternate_phone_number
 * @property string $nationality
 * @property string $email
 * @property string $password
 * @property string $dob
 * @property string|null $pan_number
 * @property int $gender_id
 * @property int $is_resigned
 * @property int $is_suspended
 * @property int $is_active
 * @property mixed|null $related_urls
 * @property mixed|null $qualifications
 * @property mixed|null $experience
 * @property \Illuminate\Support\Carbon|null $email_verified_at
 * @property string $avatar
 * @property string $activation_token
 * @property string|null $updated_by
 * @property string|null $created_by
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property string|null $remember_token
 * @property \Illuminate\Support\Carbon|null $deleted_at
 * @property-read \Illuminate\Database\Eloquent\Collection|\Laravel\Passport\Client[] $clients
 * @property-read int|null $clients_count
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\Designation[] $designations
 * @property-read int|null $designations_count
 * @property-read string $avatar_url
 * @property-read \Illuminate\Notifications\DatabaseNotificationCollection|\Illuminate\Notifications\DatabaseNotification[] $notifications
 * @property-read int|null $notifications_count
 * @property-read \App\Models\Role $roles
 * @property-read \Illuminate\Database\Eloquent\Collection|\Laravel\Passport\Token[] $tokens
 * @property-read int|null $tokens_count
 * @method static bool|null forceDelete()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\User newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\User newQuery()
 * @method static \Illuminate\Database\Query\Builder|\App\Models\User onlyTrashed()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\User query()
 * @method static bool|null restore()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\User whereActivationToken($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\User whereAddress($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\User whereAlternatePhoneNumber($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\User whereAvatar($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\User whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\User whereCreatedBy($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\User whereDeletedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\User whereDob($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\User whereEmail($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\User whereEmailVerifiedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\User whereEmployeeCode($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\User whereExperience($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\User whereFirstName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\User whereGenderId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\User whereIsActive($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\User whereIsResigned($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\User whereIsSuspended($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\User whereLastName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\User whereNationality($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\User wherePanNumber($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\User wherePassword($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\User wherePhoneNumber($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\User whereQualifications($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\User whereRelatedUrls($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\User whereRememberToken($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\User whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\User whereUpdatedBy($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\User whereUserId($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\User withTrashed()
 * @method static \Illuminate\Database\Query\Builder|\App\Models\User withoutTrashed()
 * @mixin \Eloquent
 */
class User extends Authenticatable
{
    use HasApiTokens, Notifiable, SoftDeletes;

    /**
     * @var string
     */
    protected $primaryKey = "user_id";

    /**
     * @var array
     */
    protected $dates = ['deleted_at'];

    /**
     * @var bool
     */
    public $incrementing = false;

    /**
     * @var string
     */
    public $keyType = "string";

    /**
     * @var array
     */
    public $fillable = [
        'user_id',
        'first_name',
        'last_name',
        'email',
        'password',
        'avatar',
        'phone_number',
        'alternate_phone_number',
        'employee_code',
        'address',
        'nationality',
        'dob',
        'pan_number',
        'gender_id',
        'related_urls',
        'is_active',
        'is_suspended',
        'is_resigned',
        'related_urls',
        'qualifications',
        'experience',
        'email_verified_at',
        'avatar',
        'activation_token',
        'updated_by',
        'created_by',
    ];

    /**
     * @var array
     */
    protected $hidden = ['password', 'remember_token', 'activation_token'];

    /**
     * @var array
     */
    protected $casts = ['email_verified_at' => 'datetime'];

    /**
     * @return string
     * @author jithinvijayan
     */
    public function getAvatarUrlAttribute()
    {
        return Storage::url('avatars/' . $this->user_id . '/' . $this->avatar);
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     * @author jithinvijayan
     */
    public function roles()
    {
        return $this->hasOne('App\Models\Role');
    }

    public function designations()
    {
        return $this->hasMany('App\Models\Designation');
    }

    public function gender()
    {
        return $this->hasOne(Gender::class);
    }
}
