<p align="center"><img src="https://gitlab.com/ovaqlab/enovalab-core/-/blob/master/logo.svg" width="400"></p>

<p align="center">
<a href="https://travis-ci.org/laravel/framework"><img src="https://travis-ci.org/laravel/framework.svg" alt="Build Status"></a>
<img src="https://img.shields.io/badge/License-Apache%202-brightgreen" alt="Apache 2 License"/>
<img src="https://img.shields.io/badge/Stable-dev-red" alt="Development Version"/> 


## About Enovolab

Enovolab is a web application that proposing following requirements:

- Simple and fast employee management
- Easy Leave Management
- Daily revenue & expenditure tracking
- Chat application for office staffs.
- Monthly profit & loss charts.
- Salary Management.

## Contributing

Thank you for considering contributing to the Enovolab! The contribution guide will be updated shortly.

## Code of Conduct

In order to ensure that the Laravel community is welcoming to all, please review and abide by the [Code of Conduct](https://laravel.com/docs/contributions#code-of-conduct).

## License

The Enovolab is open-sourced software licensed under the [Apache 2](https://github.com/itsmeJithin/enovolab/blob/master/LICENSE).

