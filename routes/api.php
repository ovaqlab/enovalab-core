<?php


/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/
Route::group(['prefix' => 'v1'], function () {
    Route::group(['prefix' => 'auth'], function () {
        Route::post('login', 'API\AuthController@login')->name("auth:login");
        Route::post('sign-up', 'API\AuthController@signup');
    });
    Route::group(['middleware' => 'auth:api'], function () {
        Route::get('logout', 'API\AuthController@logout');
        Route::get('user', 'API\AuthController@user');
        Route::get('get-genders', 'API\HelperController@getGenders');
        Route::get('get-nationalities', 'API\HelperController@getNationalities');
        Route::post('add-team-member', 'API\HelperController@addTeamMember');
    });
});
